﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tasks.DoNotChange;

namespace Tasks
{
    public class DoublyLinkedList<T> : IDoublyLinkedList<T>
    {
        public Item<T> Head { get; private set; }
        public int Length
        {
            get;

            private set;
        }


        public void Add(T e)
        {
            if (e is null) throw new ArgumentNullException("e");
            if (Head is null && Length == 0)
            {
                Head = new Item<T>(e);
                Length++;
                return;
            }
            var newItem = new Item<T>(e);
            var tail = ElementOnIndex(Length - 1);
            tail.Next = newItem;
            newItem.Previous = tail;
            Length++;
        }

        public void AddAt(int index, T e)
        {
            if (e is null) throw new ArgumentNullException("e");
            if (index < 0 || index > Length) throw new IndexOutOfRangeException();
            if (index == 0)
            {
                Item<T> temp = Head;
                Head = new Item<T>(e);
                Head.Next = temp;
                temp.Previous = Head;
                Length++;
            }
            else if (index == Length)
            {
                Add(e);
            }
            else
            {
                Item<T> current = ElementOnIndex(index);
                var newItem = new Item<T>(e)
                {
                    Next = current,
                    Previous = current.Previous
                };
                current.Previous.Next = newItem;
                current.Previous = newItem;
                Length++;
            }
        }

        public T ElementAt(int index)
        {
            return ElementOnIndex(index).Data;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new CustomListEnumerator<T>(Head);
        }

        public void Remove(T item)
        {
            if (item is null) throw new ArgumentNullException("item");
            if (Head.Data.Equals(item))
            {
                Head = Head.Next;
                Length--;
                return;
            }
            Item<T> current = Head.Next;
            for (int i = 1; i < Length; i++)
            {
                if (current.Data.Equals(item))
                {
                    current.Previous.Next = current.Next;
                    if (i != Length - 1)
                        current.Next.Previous = current.Previous;
                    Length--;
                    return;
                }
                current = current.Next;
            }
        }

        public T RemoveAt(int index)
        {
            if (index < 0 || index >= Length) throw new IndexOutOfRangeException("index");
            if (index == 0)
            {
                var data = Head.Data;
                Head = Head.Next;
                Length--;
                return data;
            }
            Item<T> current = ElementOnIndex(index);
            current.Previous.Next = current.Next;
            if (index != Length - 1)
                current.Next.Previous = current.Previous;
            Length--;
            return current.Data;

        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        Item<T> ElementOnIndex(int index)
        {
            if (index < 0 || index >= Length) throw new IndexOutOfRangeException();
            Item<T> current = Head;
            for (int i = 0; i < index; i++)
                current = current.Next;
            return current;
        }

    }
}
