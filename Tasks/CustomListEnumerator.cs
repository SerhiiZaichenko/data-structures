﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Tasks
{
    internal class CustomListEnumerator<T> : IEnumerator<T>
    {
        readonly Item<T> head;
        Item<T> current;
        public T Current => current == null ? throw new InvalidOperationException() : current.Data;

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
        }
        public bool MoveNext()
        {
            if (current is null)
            {
                current = head;
                return true;
            }
            if (current.Next is null)
            {
                Reset();
                return false;
            }
            current = current.Next;
            return true;
        }

        public void Reset()
        {
            current = null;
        }
        public CustomListEnumerator(Item<T> head)
        {
            if (head is null) throw new ArgumentNullException("head");
            this.head = head;
        }
    }
}
